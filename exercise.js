// Теоретичні питання

// 1. Опишіть своїми словами, що таке метод об'єкту.
  
// Метод об'єкту - це функція, яка є значенням свойству об'єкту.Вона слугує для роботи з даними 
// об'єкту.

// 2. Який тип даних може мати значення властивості об'єкта?

// Любий тип даних: це можуть бути як примітивні типи даних, булі, так і самі об'єкти, масиви, 
// функції.
        
// 3. Об'єкт це посилальний тип даних. Що означає це поняття?

// Це означає, що об'єкт зберігається та копіюється по ссилці. Тобто, при копіюванні об'єкту 
// ми отримуємо посилання на цей же об'єкт, а сам об'єкт не дублюється.При звертанні до
// властивості об'єкту, виконується операція із самим об'єктом.


//     Завдання

/*Реалізувати функцію створення об'єкта "юзер". Завдання має бути виконане на чистому 
Javascript без використання бібліотек типу jQuery або React.

Технічні вимоги:
Написати функцію createNewUser(), яка буде створювати та повертати об'єкт newUser.
При виклику функція повинна запитати ім'я та прізвище.
Використовуючи дані, введені юзером, створити об'єкт newUser з властивостями firstName 
та lastName.
Додати в об'єкт newUser метод getLogin(), який повертатиме першу літеру імені юзера, 
з'єднану з прізвищем, все в нижньому регістрі (наприклад, Ivan Kravchenko → ikravchenko).
Створити юзера за допомогою функції createNewUser(). Викликати у цього юзера функцію 
getLogin(). Вивести у консоль результат виконання функції.
Необов'язкове завдання підвищеної складності
Зробити так, щоб властивості firstName та lastName не можна було змінювати напряму. 
Створити функції-сеттери setFirstName() та setLastName(), які дозволять змінити 
дані властивості.*/


function createNewUser() {
  let firstName = prompt("Enter your name:");
  let lastName = prompt("Enter your surname:");

  const newUser = {
    get firstName() {
      return firstName;
    },
    get lastName() {
      return lastName;
    },
    getLogin: function () {
      return `${this.firstName[0]}${this.lastName}`.toLowerCase();
    },
    set setFirstName (value) {
      firstName = value;
    },
    set setLastName (value) {
      lastName = value;
    },
  };
    return newUser;
  
}

const userLogin = createNewUser();
  
console.log(userLogin.getLogin());

userLogin.setFirstName ="Anna";
userLogin.setLastName="Yarmolenko";

console.log(userLogin.firstName);
console.log(userLogin.lastName);
  
